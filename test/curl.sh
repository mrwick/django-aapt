HOST=localhost
if [[ -z $1 ]]
then
    curl $HOST:8000/api/applications/ \
        > get.curl.log 2>&1
else
    curl \
        -F application=@$1 \
        $HOST:8000/api/applications/ \
        > post.curl.log 2>&1
fi

#         -F package_name='pkg' \
#         -F package_version_code='9' \

#         -H "Content-Disposition: attachment; filename=X.apk" \
#         -H "Content-Type: application/apk" \
#         -d @$1 \

# curl -X DELETE localhost:8000/api/application/2/
