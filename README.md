# django-aapt

learning django and using aapt

the server will be served to the default 127.0.0.0:8000
if changing to 0.0.0.0:8000, `settings.ALLOWED_HOSTS` will need to be adjusted
this is still set to be a dev server

## starting the server

```
cd django_aapt
./manage.py makemigrations
./manage.py migrate
./manage.py runserver
```
## testing the server (assuming on localhost:8000)

```
cd test
 curl.sh ShatteredPD-v0.9.1b-Android.apk ; cat post.curl.log
. curl.sh ; cat get.curl.log
```

### info

The initial desire was to use a serializer purely for the file upload, and a model only to store information, but in the end it was easier to split the actions between two serialisers.

Another desire was to have a function based view per REST operation, melding them all into one url, but that also was easier to implement in one function that called another for other operations. The class based views and the router are awesome, it was just a challenge to learn more about django and drf.

This is tested with a local machine, simply using the same apk, the validation only checks the filename, but it should really peek inside the file and get a magic number or something.
