from django.apps import AppConfig

class ApkApiConfig(AppConfig):
    name = 'apk_api'
