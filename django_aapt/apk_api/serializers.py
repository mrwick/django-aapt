'''
'''

from rest_framework import serializers
from .models import AppModel

class AppSerializer(serializers.ModelSerializer):
    application = serializers.FileField(max_length=200)

    def validate_application(self, value):
        if not value.name.endswith('.apk'):
            raise serializers.ValidationError(
                    'not an .apk file')
        return value

    class Meta:
        model = AppModel
        fields = ('application',)

class AppsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppModel
        fields = (
                'application',
                'package_name',
                'package_version_code',
                )
