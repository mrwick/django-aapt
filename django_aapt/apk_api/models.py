import os
import re
from django.db import models
from django.conf import settings
from aapt2 import aapt

class AppModel(models.Model):
    application = models.FileField(max_length=200)
    package_name = models.CharField(max_length=200)
    package_version_code = models.CharField(max_length=40)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        badging = aapt_dump_badging(self.application.path)
        package_version = get_package_version(badging)
        self.package_name = package_version[0]
        self.package_version_code = package_version[1]

        super().save(
                *args,
                force_update=True,
                force_insert=False,
                )

def aapt_dump_badging(apk_file_path):
    return aapt.dump(apk_file_path, 'badging')

def get_package_version(badging):
    match = re.compile(
            "package: name='(\\S+)' "
            "versionCode='(\\d+)' "
            "versionName='(\\S+)'").match(badging)

    return (match.group(1), match.group(2))
