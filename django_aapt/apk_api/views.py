from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import AppModel
from .serializers import AppSerializer, AppsSerializer


@api_view(['GET', 'POST'])
def apk_api_view(request):
    if request.method == 'POST':
        return add_app(request)
    elif request.method == 'GET':
        return list_apps(request)
    else:
        return Response(
                'not implemented',
                status=status.HTTP_400_BAD_REQUEST)

def add_app(request):
    app_serialiser = AppSerializer(data=request.data)
    if app_serialiser.is_valid():
        app_serialiser.save()
        return Response(
                'uploaded '
                f"{app_serialiser.data['application']}",
                status=status.HTTP_201_CREATED)
    return Response(
            app_serialiser.errors,
            status=status.HTTP_400_BAD_REQUEST)

def list_apps(request):
    apps = AppModel.objects.all()
    apps_serialiser = AppsSerializer(
            apps,
            context={'request': request},
            many=True)
    return Response(apps_serialiser.data)
